clear all;
load('input_data.mat'); 

%convert matrices to vectors
vecE = E(:);
vecEBP = EBP(:);
vecH = H(:);
vecHBP = HBP(:);
vecM = M(:);
vecMBP = MBP(:);
vecT = T(:);
vecTBP = TBP(:);
vecZero = zero(:);
vecZeroBP = zeroBP(:);
EHMTO = [E,H,M,T,zero];
EHMTO_BP = [EBP,HBP,MBP,TBP,zeroBP];

% create input X, and desired output Y matrices
X = [vecEBP,vecHBP,vecMBP,vecTBP,vecZeroBP];
Y = X;

%hyper parameters for the learning algorithm
n = 100000; %number of training epochs
mu = 1e-3;   %learning rate
tol = 1e-14;  %tolerance. Training stops either when tol is reached or n, whichever is early.

%train the network
Mmem = corrmm(X,Y,mu,n,tol); 

%recalled output from trained network
Y_recall = reshape(Mmem*X,12,12,5);
Y_diff = reshape(Y,12,12,5) - Y_recall;

EHMTO_recall = [Y_recall(:,:,1),Y_recall(:,:,2),Y_recall(:,:,3),Y_recall(:,:,4),Y_recall(:,:,5)];
EHMTO_diff   = [Y_diff(:,:,1),Y_diff(:,:,2),Y_diff(:,:,3),Y_diff(:,:,4),Y_diff(:,:,5)];
EHMTO_threshold = EHMTO_recall;
EHMTO_threshold(EHMTO_threshold<0) = -1;
EHMTO_threshold(EHMTO_threshold>0) =  1;

%calculate average errors
avg = @(inp) ( norm(inp(:),1)/length(inp(:)));
err_avg_E = avg(Y_diff(:,:,1)); 
err_avg_H = avg(Y_diff(:,:,2)); 
err_avg_M = avg(Y_diff(:,:,3)); 
err_avg_T = avg(Y_diff(:,:,4)); 
err_avg_O = avg(Y_diff(:,:,5));
display('Average absolute difference (average of absolute pixel values in the Difference images):');
fprintf('E : %g, H: %g, M: %g, T: %g, O:%g\n',err_avg_E,err_avg_H,err_avg_M,err_avg_T,err_avg_O);

%calculate mismatch errors
per_err = @(index) (nnz(reshape(Y(:,index),12,12) - EHMTO_threshold(:,12*(index-1)+1:12*index)) / 1.44);
err_per_E = per_err(1);
err_per_H = per_err(2);
err_per_M = per_err(3);
err_per_T = per_err(4);
err_per_O = per_err(5);
display('A more meaningful measure is the % of mismatched pixels in the Thresholded Recalled images:');
fprintf('E : %g, H: %g, M: %g, T: %g, O:%g\n',err_per_E,err_per_H,err_per_M,err_per_T,err_per_O);

%plot the results
figure(1);
subplot(4,1,1);
imagesc(EHMTO_BP,[min(min(EHMTO_BP)),max(max(EHMTO_BP))]), colormap(gray), colorbar;
set(gca,'xtick',[]), set(gca,'ytick',[]);
title('Uncorrupted Training Inputs and Desired Outputs');
subplot(4,1,2);
imagesc(EHMTO_recall,[min(min(EHMTO_recall)),max(max(EHMTO_recall))]), colormap(gray), colorbar;
set(gca,'xtick',[]), set(gca,'ytick',[]);
title('Recalled Images');
subplot(4,1,3);
imagesc(EHMTO_diff,[min(min(EHMTO_diff)),max(max(EHMTO_diff))]), colormap(gray), colorbar;
set(gca,'xtick',[]), set(gca,'ytick',[]);
title('Difference between Desired images and Recalled images');
subplot(4,1,4);
imagesc(EHMTO_threshold,[min(min(EHMTO_threshold)),max(max(EHMTO_threshold))]), colormap(gray), colorbar;
set(gca,'xtick',[]), set(gca,'ytick',[]);
title('Thresholded Recalled images');
saveas(gcf,'fig3a.png');
